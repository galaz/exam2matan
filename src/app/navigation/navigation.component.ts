import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit {

  constructor(private router:Router) {}

  logout(){ 
      localStorage.removeItem('token');      
      this.router.navigate(['/login']);
  }

  ngOnInit() {
  	var value = localStorage.getItem('token');
    
    if(!value || value == undefined || value == "" || value.length == 0){    
      this.router.navigate(['/login']);
    }
  }

}
