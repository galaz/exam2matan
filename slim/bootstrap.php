<?php

require 'config/credentials.php';
require 'vendor/autoload.php';

Use \Illuminate\Database\Capsule\Manager as Capsule;

$capsule = new Capsule();

$capsule->addConnection([
   'driver' => 'mysql',
   'host' => $db_host,
   'database' => $db_name,
   'username' => $db_user,
   'password' => $db_password,
   'charset'  => 'utf8',
   'collation' => 'utf8_unicode_ci', 
   "prefix" => '',
]);

//CREATE TABLE `users` (
//    `id` int(10) NOT NULL PRIMARY KEY AUTO_INCREMENT,
//    `name` varchar(225),
//    `phone` varchar(225),
//    `updated_at` datetime,
//    `created_at` datetime
//) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

$capsule->bootEloquent();


  